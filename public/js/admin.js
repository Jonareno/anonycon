// var GlobaljQueryGridExtensionMethods = (function(){
// 	var api = {};

// 	api.test_callback = function(container, btnElem){
//     	alert("pony!");
//     }
//     return api;
// })();

function add_link_to_hero_element(container, btnElem){
    	var link_target = prompt("Link to:", "#");
    	if (link_target === "#" || link_target === '')
    		$(container).removeClass("hero-link").removeAttr("data-link");
    	else
    		$(container).addClass("hero-link").attr("data-link", link_target);

}

function link_to_icon_dir(){
	window.open('http://fontawesome.io/icons/', '_blank');
}

// tinyMCE.init({
// 	selector: '.gm-content',
// 	valid_elements : "+*[*]",
// 	content_css: '/css/anonycon.css,https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css, https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
// 	plugins: "code wordcount",
// 	toolbar: "code",
// 	menubar: "tools"
// });

$(function(){

	$("input[type='datetime']").appendDtpicker();
	
	$(".jq-grid-manager:not(.hero-grid-manager)").gridmanager({
		colCustomClasses: [],
		rowCustomClasses: ['content-section'],
		controlButtons: [[12], [4,8], [8,4]]
    });
	$(".jq-grid-manager.hero-grid-manager").gridmanager({
		colCustomClasses: ['active', 'pending', 'closed'],
		rowCustomClasses: [],
		addDefaultColumnClass: true,
		colClass: 'hero-action-btn',
		controlButtons: [[12], [6,6], [4,8], [8,4], [4,4,4]],
		customControls: {
            // global_row: [{ callback: 'test_callback', loc: 'bottom', btnLabel: 'row btn', iconClass: 'fa fa-link' }],
            global_col: [
            	{ callback: 'add_link_to_hero_element', loc: 'top', iconClass: 'fa fa-link' },
            	{ callback: "link_to_icon_dir", loc: 'top', iconClass: 'fa fa-flag'}
            ]
        }
    });


    $(".form-confirm").submit(function(){
        return confirm('Are you sure you wish to delete this record?');
    });
});

