$(function () {
	$('.animate-this').attr('class', 'animate-this');//fixes bubg with wysiwyg
	// $('[data-toggle="tooltip"]').tooltip();

	$(".hero-link").click(function () {
		var aTag = $($(this).data("link"));
		$('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
	});

	$('#discord-widget-toggle').on('shown.bs.collapse', function () {
		$(this).parent().find(".fa").removeClass("fa-weixin").addClass("fa-times");
	}).on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa").removeClass("fa-times").addClass("fa-weixin");
	});

	$('#scroll-down-link').mouseover(function(){
		$('#scroll-down-link > i').removeClass('bounce animated');
	});

	
	$("svg#Layer_1").css('visibility', 'visible');
	if (window.Vivus)
		new Vivus('Layer_1', {
			type: 'delayed',
			duration: 200,
			start: 'autostart',
			dashGap: 20,
			forceRender: false
		},
		function () {
			if (window.console) {
				console.log('Animation finished. [log triggered from callback]');
			}
			$("svg#Layer_1 path").css({
				'fill': '#fff',
				'stroke': 'transparent'
			});
		});

	$(window).scroll(function () {
		if (window.scrollY > window.innerHeight) {
			$("body").addClass("navbar-fixed"); 
		} else 
		{
			$("body").removeClass("navbar-fixed"); 
		}

		$('.animate-this').each(function(i, e){
			// debugger;
			if (UTILITY.isElementInView($(e), false)) {
				$(e).addClass($(e).data('animation'));
			}
		})
		
	});


	$(".color-trail").mouseenter(function () {
		$(this).addClass("color-on");
		$(this).removeClass("color-off");
	});
	$(".color-trail").mouseleave(function () {
		$(this).removeClass("color-on");
		$(this).addClass("color-off");
	});



	(function initDelayedOpening() {

		function getCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}
		if (getCookie("conDelayedUntillDec2019") == '') {
			$('#delayedOpeningModal').modal('toggle');
			var d = new Date();
			d.setTime(d.getTime() + (90 * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			document.cookie = "conDelayedUntillDec2019" + "=" + true + ";" + expires + ";path=/";
		}

	})();

});

var UTILITY = (function () {
	var publicApi = {};
	var _locals = {
		$navCheeseburgerIcon: $("#master-nav-collapse-trigger"),
		_this: this
	}

	publicApi.ScrollToCenter = function (destination) {
		// accepts id for target DOM with #
		var $destination = $(destination);
		if (!$destination.length)
			return;
		var destinationHeight = $destination.height();
		var windowHeight = $(window).height();
		var navheight = $("nav.navbar-fixed-top").height();
		var destinationPosY = $destination.offset().top;
		var animationSpeed = 400;
		var scrollOffset = ((windowHeight - destinationHeight - navheight) / 2);
		var scrollToCenterDestination = destinationPosY - scrollOffset - navheight
		var scrollToY = (destinationHeight < windowHeight ? scrollToCenterDestination : (destinationPosY - navheight))
		$('html, body').animate({
			scrollTop: scrollToY
		}, animationSpeed);
	};

	publicApi.ToggleMobileNav = function () {
		_locals.$navCheeseburgerIcon.trigger("click");
	}

	publicApi.MasterNavItemClick = function (destination) {
		this.ScrollToCenter(destination);
		if (_locals.$navCheeseburgerIcon.css("display") != "none")
			this.ToggleMobileNav();
	}

	publicApi.isElementInView = function (element, fullyInView) {
		var pageTop = $(window).scrollTop();
		var pageBottom = pageTop + $(window).height();
		var elementTop = $(element).offset().top;
		var elementBottom = elementTop + $(element).height();

		if (fullyInView === true) {
			return ((pageTop < elementTop) && (pageBottom > elementBottom));
		} else {
			return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
		}
	}

	$(function () {
		if (window.location.hash) {
			UTILITY.ScrollToCenter(window.location.hash);
		}
	})


	return publicApi;
})();