<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
	'uses' => 'HomeController@getHome',
	'as' => 'home.index'
]);

Route::get('/rules',  [
	'uses' => 'HomeController@getRules',
	'as' => 'home.rules'
]);


Route::group(['prefix' => 'admin'], function(){
	Route::group(['middleware' => 'guest'], function(){
		Route::get('/', [
			'uses' => 'AdminController@getLogin',
			'as' => 'admin.login'	
		]);
		
		Route::post('/', [
			'uses' => 'AdminController@postLogin',
			'as' => 'admin.login'	
		]);
		
	});
	
	Route::group(['middleware' => 'auth'], function(){
		Route::get('/logout', [
			'uses' => 'AdminController@getLogout',
			'as' => 'admin.logout'
		]);
		Route::get('/dashboard', [
			'uses' => 'AdminController@getDashboard',
			'as' => 'admin.dashboard'
		]);

		Route::get('/register', [
			'uses' => 'AdminController@getRegister',
			'as' => 'admin.register'
		]);
		
		Route::post('/register', [
			'uses' => 'AdminController@postRegister',
			'as' => 'admin.register'
		]);
		
		
		Route::get('/states', [
			'uses' => 'SiteStateController@getStates',
			'as' => 'admin.states'
		]);
		Route::get('/states/create', function(){
		    return redirect()->route('admin.stateEdit', ['id' => 0]);
        })->name('admin.stateCreate');
		Route::get('/states/edit/{id}', [
			'uses' => 'SiteStateController@getEditState',
			'as' => 'admin.stateEdit'
		]);
		Route::post('/states/edit/{id}', [
			'uses' => 'SiteStateController@postEditState',
			'as' => 'admin.stateEdit'
		]);				
		Route::post('/states/delete/{id}', [
			'uses' => 'SiteStateController@postDeleteStates',
			'as' => 'admin.stateDelete'
		]);
		
		
		
		Route::get('/sections', [
			'uses' => 'SiteSectionController@getSections',
			'as' => 'admin.sections'
		]);
		Route::get('/sections/create', function(){
            return redirect()->route('admin.sectionEdit', ['id' => 0]);
        })->name('admin.sectionCreate');;
		Route::get('/sections/edit/{id}', [
			'uses' => 'SiteSectionController@getEditSection',
			'as' => 'admin.sectionEdit'
		]);
		Route::post('/sections/edit/{id}', [
			'uses' => 'SiteSectionController@postEditSection',
			'as' => 'admin.sectionEdit'
		]);
		Route::post('/sections/copy/{id}', [
			'uses' => 'SiteSectionController@postCopySection',
			'as' => 'admin.sectionCopy'
		]);
		Route::post('/sections/delete/{id}', [
			'uses' => 'SiteSectionController@postDeleteSection',
			'as' => 'admin.sectionDelete'
		]);

		Route::get('/timer', [
			'uses' => 'CountDownTimerController@latest',
			'as' => 'admin.timer'
		]);
		Route::post('/timer/update/{id}', [
			'uses' => 'CountDownTimerController@edit',
			'as' => 'admin.timerEdit'
		]);
	});		
});
