<?php

namespace App\Http\Controllers;

use App\User;
use App\SiteState;
use App\SiteSection;
use Illuminate\Http\Request;
use Auth;	
use App\Http\Requests;
use Validator;

class SiteStateController extends Controller
{
    public function getCreateState(){
		return Redirect()->route('admin.stateEdit', ['id' => 0]);
	}
	
	public function getStates(){
		$states = SiteState::where('isDeleted', '=', 0)->get();
		return view('admin.states', ['sitestate' => $states]);
	}
	
	public function getEditState($id){
		if ($id < 1){
			$state = new SiteState();
		} else {
			$state = SiteState::findOrFail($id);	
		}			
		return view('admin.stateEdit', ['state' => $state]);
	}
	
	public function postEditState($id, Request $request){
		if ($id < 1){
			$state = new SiteState();
		} else {
			$state = SiteState::findOrFail($id);
		}

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'activeOnDate' => 'required|date'
        ]);
        if ($validator->fails()) {
            return view('admin.stateEdit', ['state' => $state])->withErrors($validator);
        }
		
		$state->name = $request->input('name');
		$state->activeOnDate = $request->input('activeOnDate');
		$state->isActive = $request->input('isActive');

		$state->save();
		return Redirect()->route('admin.stateEdit', ['id' => $state->id ]);
	}

	public function postDeleteStates($id, Request $request){
        $state = SiteState::findOrFail($id);
        $state->isDeleted = true;
        $state->save();

        return Redirect()->route('admin.states');
    }
}
