<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\CountDownTimer;
use Auth;

class CountDownTimerController extends Controller
{
    public function latest(){
        
        $countDownTimer = CountDownTimer::where('isDeleted', '=', 0)
            ->orderBy('id', 'DESC')
            ->first();

        if ($countDownTimer == null || !$countDownTimer->count()) {
            $countDownTimer = new CountDownTimer();
        }

        return $countDownTimer;
    }

    public function edit($id, Request $request){
        if ($id < 1){
            $timer = new CountDownTimer();
        } else {
            $timer = CountDownTimer::findOrFail($id);
        }

        $validator = Validator::make($request->all(), [
            'targetDate' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.dashboard')
                ->withErrors($validator);
        }


        $timer->name = $request->input('name');
        $timer->targetDate = $request->input('targetDate');
        $timer->description = $request->input('description');
        $timer->isActive = $request->input('isActive');
        $timer->isDeleted = $request->input('isDeleted');

        $timer->save();

        return redirect()
            ->route('admin.dashboard');

    }
}
