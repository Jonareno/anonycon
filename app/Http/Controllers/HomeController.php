<?php

namespace App\Http\Controllers;

use App\SiteSection;
use App\SiteState;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Config;

class HomeController extends Controller
{
    public function getHome(){
	    $currentSiteStates = SiteState::where('isDeleted', 0)
            ->whereDate('activeOnDate', '<', Carbon::today()->toDateString())
            ->orderBy('activeOnDate', 'desc')
            ->take(5)->get();
/**/
        $mainContent = null;
	    foreach ($currentSiteStates as $currentSiteState){
            $mainContent = SiteSection::where('isDeleted', 0)
                ->where('sectionType', Config::get('enums.section_types')["main_content"])
                ->where('activeInState', $currentSiteState->id )
                ->first();
            if ($mainContent != null){
                break;
            }
        }

        $heroSection = null;
        if ($mainContent != null){
            foreach ($currentSiteStates as $currentSiteState){
                $heroSection = SiteSection::where('isDeleted', 0)
                    ->where('sectionType', Config::get('enums.section_types')["hero_content"])
                    ->where('activeInState', $mainContent->activeInState )
                    ->first();
                if ($heroSection != null){
                    break;
                }
            }
        }

        $timerController = new CountDownTimerController();
        $timer = $timerController->latest();


	    return view('home.index', ['section' => $mainContent, 'heroContent' => $heroSection, 'timer' => $timer]);
    }
    
    public function getRules(){
	    return view('home.rules');
    }
}
