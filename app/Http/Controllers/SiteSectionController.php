<?php

namespace App\Http\Controllers;
	
use App\User;
use Validator;
use App\SiteState;
use App\SiteSection;
use Illuminate\Http\Request;
use Auth;	
use App\Http\Requests;
use Config;

class SiteSectionController extends Controller
{
    public function getSections(){
		$siteSections = SiteSection::where('isDeleted', '=', 0)
            ->where('sectionType', Config::get('enums.section_types')["main_content"])
            ->get();
		return view('admin.sections', ['sitesections' => $siteSections]);
	}
	
	public function getCreateSection(){
		return Redirect()->route('admin.sectionEdit', ['id' => 0]);
	}
	
	public function getEditSection($id){
		if ($id < 1){
			$siteSection = new SiteSection();
		} else {
			$siteSection = SiteSection::findOrFail($id);
		}	
		
		$siteStates = SiteState::where('isDeleted', '=', 0)->get();


        $heroContent = SiteSection::where('activeInState',$siteSection->activeInState)
            ->where('sectionType', Config::get('enums.section_types')["hero_content"])
            ->first();
		
		return view('admin.sectionEdit', ['section' => $siteSection, 'siteStates' => $siteStates, 'heroContent' => $heroContent]);
	}
	
	public function postEditSection($id, Request $request){
		if ($id < 1){
			$section = new SiteSection();
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:site_sections',
                'content' => 'required',
            ]);
		} else {
			$section = SiteSection::findOrFail($id);
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'content' => 'required',
            ]);
		}

        if ($validator->fails()) {
            $siteStates = SiteState::where('isDeleted', '=', 0)->get();
            return view('admin.sectionEdit', ['section' => $section, 'siteStates' => $siteStates])->withErrors($validator);
        }
		
		$section->name = $request->input('name');
		$section->content = $request->input('content');
		$section->activeInState = $request->input('activeInState');

		$section->save();

//		Handle Hero Content if it's there
//		On second observation, this kinda sucks. I should fix it.
        if ( $request->input('content_hero') != ''){
            $heroContent = SiteSection::where('activeInState',$section->activeInState)
                ->where('sectionType', Config::get('enums.section_types')["hero_content"])
                ->first();
            if($heroContent == null) {//if hero content is set
                $heroContent = new SiteSection();
            }
            $heroContent->name = $request->input('name') . " - Hero";
            $heroContent->content = $request->input('content_hero');
            $heroContent->activeInState = $section->activeInState;
            $heroContent->sectionType = Config::get('enums.section_types')["hero_content"];

            $heroContent->save();
        }

		return Redirect()->route('admin.sectionEdit', ['id' => $section->id ]);
	}

	public function postCopySection($id, Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:site_sections'
		]);
		if ($validator->fails()) {
			return redirect()->route('admin.sections')->withErrors($validator);
		}
		
		$oldSection = SiteSection::findOrFail($id);
		$newSection = new SiteSection();

		$newSection->name = $request->input('name');
		$newSection->content = $oldSection->content;
		$newSection->activeInState = $oldSection->activeInState;
		$newSection->save();

		return Redirect()->route('admin.sectionEdit', ['id' => $newSection->id ]);
	}
	
	public function postDeleteSection($id, Request $request){
		$section = SiteSection::findOrFail($id);
		$section->isDeleted = true;
		$section->save();
		
		return Redirect()->route('admin.sections');
	}
}
