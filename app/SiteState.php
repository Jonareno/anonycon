<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SiteState extends Model
{
    protected $fillable = [
	    'name','activeOnDate','isActive'
    ];
    protected $hidden = [
	    'id', 'isDeleted','createdBy','lastChangedBy'
    ];

    protected $attributes = array(
        'id' => '0',
        'isActive' => '1'
    );

    protected $guarded = [ 'id'];

    protected $casts = [
        'isActive' => 'boolean',
        'isDeleted' => 'boolean'
    ];

    public function displayName(){
	    return $this->name . " (" . $this->displayActiveOnDateTime() . ")";
    }

    public function displayActiveOnDateTime(){
	    return date('M d Y, h:m', strtotime($this->activeOnDate));
    }

    public function createdByUser(){
	    return $this->belongsTo('App\User', 'createdBy');
    }

    public function lastChangedByUser(){
	    return $this->belongsTo('App\User', 'lastChangedBy');
    }

    public function save(array $options = array())
    {
        if( !$this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
        }
        if (!$this->lastChangedBy){
            $this->lastChangedBy = Auth::user()->id;
        }
        parent::save($options);
    }
}
