<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SiteSection extends Model
{
    protected $fillable = [
        'name', 'content', 'sectionType','isDeleted', 'isActive','activeInState'
    ];

    protected $attributes = [
        'id' => '0'
    ];

    protected $hidden = [
	    'id', 'isDeleted', 'isActive', 'createdBy', 'lastChangedBy', 'activeInState'
    ];

    protected $guarded = [ 'id'];

    protected $casts = [
        'isActive' => 'boolean',
        'isDeleted' => 'boolean'
    ];

    public function activeState(){
	    return $this->belongsTo('App\SiteState', 'activeInState');
    }
    
    public function stateDisplayName(){
	    return $this->activeState->displayName();
    }
        
    public function createdByUser(){
	    return $this->belongsTo('App\User', 'createdBy');
    }
    
    public function lastChangedByUser(){
	    return $this->belongsTo('App\User', 'lastChangedBy');
    }

    public function save(array $options = array())
    {
        if( !$this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
        }
        if (!$this->lastChangedBy){
            $this->lastChangedBy = Auth::user()->id;
        }
        parent::save($options);
    }
}