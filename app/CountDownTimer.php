<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DateTime;

class CountDownTimer extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'name', 'targetDate', 'description', 'isDeleted', 'isActive'
    ];

    protected $attributes = [
        'id' => '0',
        'isActive' => '0'
    ];

    protected $hidden = [
	    'id', 'isDeleted', 'isActive', 'createdBy', 'lastChangedBy'
    ];

    protected $guarded = [ 'id'];

    protected $casts = [
        'isActive' => 'boolean',
        'isDeleted' => 'boolean'
    ];

    public function createdByUser(){
	    return $this->belongsTo('App\User', 'createdBy');
    }
    
    public function lastChangedByUser(){
	    return $this->belongsTo('App\User', 'lastChangedBy');
    }

    public function save(array $options = array())
    {
        if( !$this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
        }
        if (!$this->lastChangedBy){
            $this->lastChangedBy = Auth::user()->id;
        }

        if( $this->isDeleted == null){
            $this->isDeleted = false;
        }

        if( !$this->targetDate){
            $this->targetDate = new DateTime();
        }
        parent::save($options);
    }
}
