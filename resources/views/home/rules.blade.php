@extends('layouts.master')

@section('body')

<div class="row">
                    <div class="col-md-8">
                        <h3>Harassment Policy</h3>
                        <p>AnonyCon is dedicated to providing a harassment-free convention experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, religion, or political views. We do not tolerate harassment of convention participants or staff  in any form. Convention participants violating these rules may be sanctioned or expelled from the convention, without a refund, at the discretion of the convention organizers.</p>
                        <p>Harassment includes offensive verbal comments related to gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, religion, political views; sexual images in public spaces, deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention. Participants asked to stop any harassing behavior are expected to comply immediately.</p>
                        <p>Exhibitors in the expo hall, sponsor or vendor booths, or similar activities are also subject to the anti-harassment policy. Any display of sexualized images, activities, or other material needs to be positioned out of public view. Booth staff (including volunteers) should not use sexualized clothing/uniforms/costumes, or otherwise create a sexualized environment.</p>
                        <p>If a participant, exhibitor or member of convention staff  engages in harassing behavior, the convention organizers may take any action they deem appropriate, including warning the o ender or expulsion from the convention, with no refund. If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact a member of convention staff  immediately, identified by the staff  shirts or by presence at the convention headquarters desk.</p>
                        <p>Convention staff will be happy to help participants contact hotel security or local law enforcement, provide escorts, or otherwise assist those experiencing harassment to feel safe for the duration of the convention. We value your attendance.</p>

                        <p>We expect participants to follow these rules at all convention venues and convention-related social events.</p>
                    </div>
                    <div class="col-md-4">
                        <h3>Help</h3>
                        <h5><strong>Convention organizers:</strong></h5>
                        <p><a href="mailto:staff@anonycon.com">staff@anonycon.com</a></p>
                        <h5><strong>Security or Harassment Concerns?</strong></h5>
                        <p>Call or text the Director: <a href="tel:9084150180">(908)-415-0180</a></p>
                        <h5><strong>Stamford Police Department<br>(non-emergency):</strong></h5>
                        <p><a href="tel:2039774444">(203)-977-4444</a></p>
                        <h5><strong>Sexual Assault Crisis Center of Eastern Connecticut:</strong></h5>
                        <p><a href="tel:8889995545">(888)-999-5545</a></p>
                        <h5><strong>Eveready Cab of Darien:</strong></h5>
                        <p><a href="tel:2036558779">(203)-655-8779</a></p>
                    </div>
                </div>
                
                @endsection