<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="AnonyCon">
    <meta name="author" content="Jonathan White">

    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ URL::asset('css/anonycon.css') }}"> @yield('styles')
    <link rel="stylesheet" href="{{ URL::asset('css/animate.min.css') }}"> @yield('styles')
    <link href="http://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
</head>

<body>
@if( array_key_exists('over_toolbar_hero', View::getSections()))
    @yield('over_toolbar_hero')
@endif
@if(Auth::check())
    <div class="alert alert-info" style="position: fixed;top: 70px;z-index: 999;">
        <p class="text-center">
            You are logged in as {{ Auth::user()->email }}. <a href="{{ route('admin.dashboard') }}">Do admin things</a>!
        </p>
    </div>
@endif
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#master-nav" aria-expanded="false" id="master-nav-collapse-trigger">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/images/AnonyCon-Logo.png" height="40" width="" alt="AnonyCon Logo">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="master-nav">
            <ul class="nav navbar-nav">
                <li>
                    <span>Games So Good, They Turn Undead</small>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right text-right">
                <li><a href="javascript:UTILITY.MasterNavItemClick('#register');">Register</a>
                </li>
                <li><a href="javascript:UTILITY.MasterNavItemClick('#submit');">Run Games</a>
                </li>
                <!-- <li><a href="javascript:UTILITY.MasterNavItemClick('#hotel');">Hotel</a> -->
                </li>
                <li><a href="javascript:UTILITY.MasterNavItemClick('#vending');">Vending</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div id="main-content" class="main-content">
        @if( array_key_exists('hero_content', View::getSections()))
        <div class="row">
            <div id="subhero-wrapper" class="col-xs-12 col-md-12">
                <div>
                    @yield('hero_content')
                </div>
            </div>
        </div>
        @endif

        @yield('body')
    </div>
</div>

<footer>
    <nav class="navbar navbar-footer">
        <div class="container">
            <p class="navbar-text">© AnonyCon {{ date("Y") }}<span class="pull-right hidden-sm hidden-md hidden-lg">
                <a href="https://discordapp.com/widget?id=315980619370856449&theme=dark" target="_blank"><i class="fa fa-weixin" aria-hidden="true"></i> AnonyCon on Discord</a>
            </span></p>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right hidden-xs">
                    <li><a href="javascript:UTILITY.ScrollToCenter('#register');">Register</a>
                    </li>
                    <li><a href="javascript:UTILITY.ScrollToCenter('#submit');">Run Games</a>
                    </li>
                    <!-- <li><a href="javascript:UTILITY.ScrollToCenter('#hotel');">Hotel</a> -->
                    </li>
                    <li><a href="javascript:UTILITY.MasterNavItemClick('#vending');">Vending</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</footer>


<div id="discord-widget" style="position: fixed;right: 0;bottom:0;" class="hidden-xs">
    <div class="collapse" id="discord-widget-toggle">
        <iframe src="https://discordapp.com/widget?id=315980619370856449&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" style="box-shadow: -3px 6px 12px rgba(5,5,50,.35); border-radius: 5px;"></iframe> 
    </div>
    <a class="btn btn-primary pull-right" role="button" data-toggle="collapse" href="#discord-widget-toggle" aria-expanded="false" aria-controls="collapseExample" style="width: 74px;height: 54px;" title="AnonyCon on Discord">
        <i class="fa fa-weixin fa-3x" aria-hidden="true"></i>
    </a>
</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script async="" type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/jquery.countdown.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="{{ URL::asset('js/global.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/vivus.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" charset="UTF-8" async="" src="http://addtocalendar.com/atc/1.5/atc.min.js"></script>
<script type="text/javascript">
 $(function() {
    @if(!empty($timer) && $timer->isActive)
    $("#countdown-wrapper").countdown("{{ $timer->targetDate }}", function(event) {
        var $this = $(this).html(event.strftime(''
        //              + '<span>%w w</span>'
        + '<span>%D d</span>'
        + '<span>%H h</span>'
        + '<span>%M m</span>'
        + '<span>%S s</span>'));
        });
    @endif
    $("#count-down-button-register").countdown("2017/09/01 12:00:00", function(event) {
        var $this = $(this).html(event.strftime('' + '<span>%Dd</span> ' + '<span>%Hh</span> ' + '<span>%Mm</span> ' + '<span>%Ss</span>'));
    });
});
</script>
<script async="" type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b207a8863c2fbc"></script>
@yield('scripts')
</body>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1288193797881275');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1288193797881275&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</html>