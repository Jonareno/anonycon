<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="AnonyCon">
		<meta name="author" content="Jonathan White">

		<title>@yield('title')</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{ URL::asset('libs/jquery.simple-dtpicker/jquery.simple-dtpicker.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('libs/jQuery-gridmanager/css/jquery.gridmanager.css') }}">
		<link rel="stylesheet" type="text/css" id="u0" href="http://cdnjs.cloudflare.com/ajax/libs/tinymce/4.1.2/skins/lightgray/skin.min.css">
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/tinymce/4.1.2/skins/lightgray/content.inline.min.css">
    <link rel="stylesheet" href="{{ URL::asset('css/admin.css') }}">
		@yield('styles')
	</head>
	<body>
		
		@if(Auth::check())
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ route('admin.dashboard') }}">AnonyCon Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <!-- <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li> -->
            <li>
              <a class="btn btn-link" href="{{ route('home.index') }}" target="_blank">
                <i class="fa fa-eye" aria-hidden="true"></i> View Site</a>
            </li>
            <li>
              <a href="#" data-toggle="tooltip" title="TODO: Add Help?" data-placement="bottom">
                <i class="fa fa-question-circle" aria-hidden="true"></i> Help
              </a>
            </li>
            <li>
              <li>
              <a class="btn btn-link" href="{{ route('admin.logout') }}">
                <i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
            </li>
            </li>
          </ul>
          <!-- <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </nav>
	@endif
	
    <div class="container-fluid">
      <div class="row">
	      @if(Auth::check())
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li {{ Request::is('admin/dashboard') ? 'class=active' : '' }}><a href="{{ route('admin.dashboard') }}">Dashboard <span class="sr-only">(current)</span></a></li>
            <li {{ Request::is('admin/states*') ? 'class=active' : '' }}><a href="{{ route('admin.states') }}">Site States</a></li>
            <li {{ Request::is('admin/sections*') ? 'class=active' : '' }}><a href="{{ route('admin.sections') }}">Site Sections</a></li>
          </ul>          
        </div>
        @endif
        
        @if(Auth::check())
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">	        
        @else
        <div class="col-md-6 col-md-offset-3 main">
        @endif
          
          @if (count($errors) > 0 )
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{ $error }}</p>
				@endforeach
			</div>
			@endif
          
          @yield('body')
          
        </div>
      </div>
    </div>
    	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="{{ URL::asset('libs/jquery.simple-dtpicker/jquery.simple-dtpicker.js') }}" type="text/javascript"></script>
		<script src="{{ URL::asset('libs/jQuery-gridmanager/js/jquery.gridmanager.js') }}" type="text/javascript"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.1.2/tinymce.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.1.2/jquery.tinymce.min.js"></script>
    <script src="{{ URL::asset('js/global.js') }}" type="text/javascript"></script>
		<script src="{{ URL::asset('js/admin.js') }}" type="text/javascript"></script>
		@yield('scripts')
	</body>
</html>