@extends('layouts.admin')

@section('body')
	<h1 class="page-header">Sections</h1>	
   <p>Sections are blocks of content which will be shown during a designated Site State, or block of time.</p>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Name
				</th>
				<th>
					Site State
				</th>
				<th>
					Actions
				</th>
			</tr>
		</thead>
	@foreach($sitesections as $sections)
		<tr>
			<td>
				{{ $sections->id }}
			</td>
			<td>
				{{ $sections->name }}
			</td>
			<td>
				{{ $sections->stateDisplayName() }}
			</td>
			<td>
				<a href="{{ route('admin.sectionEdit', ['id' => $sections->id ]) }}" class="btn">Edit</a>
				
				<form method="post" action="{{ route('admin.sectionCopy', ['id' => $sections->id ]) }}" class="form-inline" style="display:inline-block">
					<button class="btn btn-link" type="button" data-toggle="modal" data-target="#copySectionNameModal" >Copy</button>
					<!-- Modal -->
					<div class="modal fade" id="copySectionNameModal" tabindex="-1" role="dialog" aria-labelledby="copySectionNameModal" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Copy to New Section</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
							<div class="form-group">
    								<label for="name">Name your new section</label>
									<input class="form-control" type="text" name="name" id="name" value="" placeholder="{{ $sections->name }} - copy" required/>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Create Copy</button>
							</div>
							</div>
						</div>
					</div>
					{{ csrf_field() }}
				</form>

				<form method="post" action="{{ route('admin.sectionDelete', ['id' => $sections->id ]) }}" class="form-inline form-confirm" style="display:inline-block">
					<button class="btn btn-link" type="submit">Delete</button>
					{{ csrf_field() }}
				</form>
			</td>
		</tr>
	@endforeach
	</table>
	<div class="text-right">
		<a href="{{ route('admin.sectionCreate') }}" class="btn btn-primary">Create</a>
	</div>
@endsection
