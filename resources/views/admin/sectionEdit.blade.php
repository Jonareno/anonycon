@extends('layouts.admin')

@section('body')
	<h2>{{ $section->id < 1 ? "Create" : "Update" }} Section {{ $section->name }}</h2>
	<div class="row">
		<div class="col-md-12">
			<form method="post" action="{{ route('admin.sectionEdit', ['id' => $section->id ]) }}" id="update-section-form">
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control" value="{{ $section->name }}">
				</div>
				@if (!isset($heroContent->content))
				<p class="text-center">
					<a href="#hero-content" class="btn-link btn-block" role="button" data-toggle="collapse">Add Hero Content</a>
				</p>
				<div id="hero-content" class="collapse">
					<div class="form-group">
						<label for="content_hero">Hero Content</label>
						<div class="jq-grid-manager hero-grid-manager well" id="hero-content-editor"></div>
						<input type="hidden" name="content_hero" value="">
					</div>
					<hr>
				</div>
				@else
					<div class="form-group">
						<label for="content_hero">Hero Content</label>
						<div class="jq-grid-manager hero-grid-manager well" id="hero-content-editor">{!! $heroContent->content !!}</div>
						<input type="hidden" name="content_hero" value="">
					</div>
				@endif
				<div class="form-group">
					<label for="content">Main Content</label>
					<div class="jq-grid-manager well" id="section-content-editor">{!! $section->content !!}</div>
					<input type="hidden" name="content" value="">
				</div>
				
				<div class="form-group">
					<label for="activeInState">Active In State</label>
					<select class="form-control" name="activeInState">
						@foreach( $siteStates as $state)
							<option value="{{ $state->id }}" {{ $state->id == $section->activeInState ? "selected" : "" }}>{{ $state->name }} ({{ date('M d Y, h:m', strtotime($state->activeOnDate) ) }})</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="createdBy">Created By</label>
					<input type="text" name="createdBy" class="form-control" disabled="disabled" value="{{ $section->createdByUser->email or "" }}">
				</div>
				<div class="form-group">
					<label for="lastChangedBy">Last Updated By</label>
					<input type="text" name="lastChangedBy" class="form-control" disabled="disabled" value="{{ $section->lastChangedByUser->email or "" }}">
				</div>
				
				<div class="text-right">
					<button type="submit" class="btn btn-primary">{{ $section->id < 1 ? "Create" : "Update" }}</button>
				</div>
					{{ csrf_field() }}
			</form>
		</div>
	</div>
                
@endsection


@section('scripts')
<script type="text/javascript">
	$(function(){
		$("#update-section-form").submit(function(){
			$('.gm-edit-mode').trigger('click');
			$(this).find("input[name='content']").val($('#section-content-editor').find('textarea').val().trim());
            $(this).find("input[name='content_hero']").val($('#hero-content-editor').find('textarea').val().trim());
        });
	});
</script>
@endsection