@extends('layouts.admin')

@section('body')
	<h2>Admin Login</h2>
	<div class="row">
		<div class="col-md-12">
			@if (count($errors) > 0 )
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{ $error }}</p>
				@endforeach
			</div>
			@endif
			<form method="post" action="{{ route('admin.login') }}">
				<div class="form-group">
					<label for="email">User Name</label>
					<input type="email" name="email" class="form-control" autofocus>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="row">
					<div class="col-md-6 text-right col-md-push-6">
						<button type="submit" class="btn btn-primary">Login</button>
					</div>
					<div class="col-md-6 col-md-pull-6">
						<a href="javascript:history.back()"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Go Back</a>
					</div>
				</div>	
					{{ csrf_field() }}
			</form>
		</div>
	</div>
                
@endsection