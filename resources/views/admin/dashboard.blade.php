@extends('layouts.admin')

@section('body')
<h1 class="page-header">Dashboard</h1>	

<div class="row">
	<div class="col-md-6">
		<p class="lead">Edit the different states the site will be in.</p>
		<a href="{{ route('admin.states') }}" class="btn btn-block btn-primary">Edit Site States</a>
	</div>
	<div class="col-md-6">
		<p class="lead">Edit the copy to be shown on the site, during a given state.</p>
		<a href="{{ route('admin.sections') }}" class="btn btn-block btn-primary">Edit Site Sections</a>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Countdown Timers</div>
				<div class="panel-body">
					<form method="post" action="{{ route('admin.timerEdit', ['id' => $timer->id ]) }}">
						<div class="form-group">
							<label for="targetDate">Count Down To</label>
							<input autocomplete="off" type="datetime" class="form-control" id="targetDate" name="targetDate" value="{{ $timer->targetDate }}">
						</div>
						<div class="form-group">
							<label for="isActive">Show Timer?</label>
								<input type="hidden" name="isActive" value="0" />
								<input type="checkbox" id="isActive" name="isActive" value="1" aria-label="..." {{ $timer->isActive ? "checked=checked" : "" }} />
							
						</div>
						<div class="text-right">
							<button type="submit" class="btn btn-default">Save</button>
						</div>	
						{{ csrf_field() }}				
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Users</div>
					<div class="panel-body">
						<a href="{{ route('admin.register') }}">Register Admin User</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection