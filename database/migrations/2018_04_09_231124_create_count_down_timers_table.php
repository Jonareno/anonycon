<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountDownTimersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('count_down_timers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->dateTime('targetDate');            
            $table->boolean('isActive')->default(true);
            $table->boolean('isDeleted')->default(false);
            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users');
            $table->integer('lastChangedBy')->unsigned();
            $table->foreign('lastChangedBy')->references('id')->on('users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('count_down_timers');
    }
}
