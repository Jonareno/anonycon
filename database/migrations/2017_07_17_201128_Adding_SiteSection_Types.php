<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingSiteSectionTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_sections', function (Blueprint $table) {
            $table->tinyInteger('sectionType')->default(Config::get('enums.section_types')["main_content"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_sections', function (Blueprint $table) {
            $table->dropColumn('sectionType');
        });
    }
}
