<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->unique();
            $table->longText('content');
            $table->integer('activeInState')->unsigned();
            $table->foreign('activeInState')->references('id')->on('site_states');
            $table->boolean('isDeleted')->default(false);
            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users');
            $table->integer('lastChangedBy')->unsigned();
            $table->foreign('lastChangedBy')->references('id')->on('users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_sections');
    }
}
